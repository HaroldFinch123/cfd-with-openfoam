**`FLOW MASTERY: Unleashing Computational Fluid Dynamics`**
===========================================================

**Course Overview**
-------------------

A comprehensive exploration of Computational Fluid Dynamics (CFD) principles, applications, and problem-solving strategies. This repository contains assignments, projects, and resources from a hands-on course, showcasing the intersection of theoretical foundations and practical expertise.

**Key Topics**
--------------

* **Computational Fluid Dynamics (CFD)**: Theoretical foundations, governing equations, and numerical methods
* **Programming Applications**:
	+ **Python**: Numerical computations and simulations
	+ **C++**: Assignments and project solutions
* **Tools and Workflows**:
	+ **OpenFoam**: CFD solver for simulation and analysis
	+ **Sublime Merge**: Version control and collaboration (Git)

**Repository Contents**
-----------------------

* **Assignments**:
	+ **Group Projects (3)**: Collaborative CFD problem-solving
	+ **Individual Assignment (1)**: Independent CFD technique application
	+ **Capstone Project**: Real-world CFD project design and simulation
* **Technical Highlights**:
	+ Numerical scheme implementation and analysis
	+ Error assessment and accuracy improvement
	+ Navier-Stokes equations solution algorithms
	+ Simulation planning and post-processing with OpenFoam

**Notable Projects**
--------------------

* **Turbulent Flow Simulation**: RANS modeling and analysis
* **Multi-phase Flow Modeling**: OpenFoam-based simulation and visualization
* **Real-world CFD Application**: Design and simulation of a practical engineering problem

**Getting Started**
-------------------

1. **Clone the Repository**:
```bash
git clone https://gitlab.com/haroldfinch/cfd-fundamentals.git
```
2. **Set up Your Environment**:
	* Install Sublime Merge (Windows): [download page](https://www.sublimemerge.com/download)
	* Familiarize yourself with OpenFoam and Python/C++ tools
3. **Explore and Learn**:
	* Review assignments and projects for insights into CFD and programming
	* Use this repository as a starting point for your own CFD endeavors

**Contact**
----------

* **LinkedIn**: [linkedin.com/in/roccagiacomo](http://www.linkedin.com/in/roccagiacomo)
