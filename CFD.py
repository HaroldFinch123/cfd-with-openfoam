# Import necessary libraries
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
from sympy import symbols, exp

# Ask NumPy to print a more reasonable number of digits when using string
# representations.
np.set_printoptions(precision=2, suppress=True)

# Definition of TDMA that is probably faster than np.linalg.sove()
def TDMA(lower_diag, main_diag, upper_diag, solution_vector):
    """
    Solve a tridiagonal linear system using the Thomas algorithm (TDMA).

    Parameters:
        lower_diag (array): Lower diagonal of the coefficient matrix.
        main_diag (array): Main diagonal of the coefficient matrix.
        upper_diag (array): Upper diagonal of the coefficient matrix.
        solution_vector (array): Right-hand side vector.

    Returns:
        array: Solution vector.
    """

    n = len(solution_vector)  # Size of the system
    w = np.zeros(n-1, float)  # Intermediate values
    g = np.zeros(n, float)    # Intermediate values
    p = np.zeros(n, float)    # Solution vector

    # Forward sweep
    w[0] = upper_diag[0] / main_diag[0]
    g[0] = solution_vector[0] / main_diag[0]

    for i in range(1, n-1):
        w[i] = upper_diag[i] / (main_diag[i] - lower_diag[i-1] * w[i-1])

    for i in range(1, n):
        g[i] = (solution_vector[i] - lower_diag[i-1] * g[i-1]) / (main_diag[i] - lower_diag[i-1] * w[i-1])

    # Back substitution
    p[n-1] = g[n-1]
    for i in range(n-1, 0, -1):
        p[i-1] = g[i-1] - w[i-1] * p[i]

    return p


# Scruture the class to solve the problem 
class ScalarTransportEqSolver:
    def __init__(self, phi_0, phi_N, N, Pe, L, theta=0, C=0.7, method='linear', save_folder='Figures/test/'):
        # Verify N
        if not isinstance(N, int) or N <= 0:
            raise ValueError("N must be a positive integer.")

        # Constructor to initialize parameters
        self._max_cycles = int(3000)
        self._cycle_count = 0
        self.method = method
        self.theta = theta
        self.phi_0 = phi_0
        self.phi_N = phi_N
        # self.phi_old = np.random.uniform(low=0.3, high=0.5, size=N)
        self.phi_old = np.zeros(N)
        self.phi_new = np.zeros(N)
        self.phi_store = np.empty((N, self._max_cycles))
        self.N = N
        self.Deltax = L/N
        #Consider making code dependant on u, gamma, rho instead of peclet
        #in case problems arise with the peclet method
        self.Deltat = np.round(C/(abs(Pe)/self.Deltax + 2/(self.Deltax**2)), decimals=6)
        self.A = np.zeros((N, N))
        self.Q = np.zeros(N)
        self.C = C
        self.Pe, self.L = Pe, L
        self.x = symbols('x')
        self.save_folder = save_folder
        

    def add_advection(self):
        """Add the advection effect to the coefficient matrix A

        Raises:
            ValueError: if self.method is not 'linear' or 'upwind'
        """
        if self.method not in ['linear', 'upwind']:
            raise ValueError("Invalid method. Choose 'linear' or 'upwind'")
        
        i = np.arange(1, self.N - 1)
        east_indices = i + 1
        west_indices = i - 1

        if self.method == 'linear':
            self.A[i, east_indices] += self.Pe / 2
            self.A[i, west_indices] -= self.Pe / 2
        elif self.method == 'upwind':
            self.A[i, east_indices] += np.minimum(self.Pe, 0)
            self.A[i, i] += abs(self.Pe)
            self.A[i, west_indices] -= np.maximum(self.Pe, 0)

    def add_diffusion(self):
        """Add the diffusion effect to the coefficient matrix A

        Raises:
            ValueError: if self.method is not 'linear' or 'upwind'
        """        
        if self.method not in ['linear', 'upwind']:
            raise ValueError("Invalid method. Choose 'linear' or 'upwind'")

        i = np.arange(1, self.N - 1)
        east_indices = i + 1
        west_indices = i - 1

        self.A[i, east_indices] -= 1 / self.Deltax
        self.A[i, i] += 2 / self.Deltax
        self.A[i, west_indices] -= 1 / self.Deltax

    def set_BC_advection(self):
        # Method to set boundary conditions
        
        if self.method == 'linear':
            # Linear method boundary conditions for the advection term 
            
            # Set coefficients for the first row in the matrix A
            self.A[0, :2] += [(self.Pe/2),
                              (self.Pe/2)]

            # Set coefficients for the last row in the matrix A
            self.A[-1, -2:] += [-(self.Pe/2),
                                -(self.Pe/2)]
            
            # Set values for the first and last elements in the vector b
            self.Q[0] += self.phi_0 * self.Pe 
            self.Q[-1] -= self.phi_N * self.Pe 
            
        elif self.method == 'upwind':
            # Upwind method boundary conditions for the advection term 
            
            # Set coefficients for the first BC in the matrix A using min and max functions
            self.A[0, :2] += [(abs(self.Pe)),
                              (min(self.Pe, 0))]

            # Set coefficients for the last BC in the matrix A using min and max functions
            self.A[-1, -2:] += [-(max(self.Pe, 0)),
                                (abs(self.Pe))]
            
            # Set values for the first BC and last BC in the vector b
            self.Q[0] += self.phi_0 * (max(self.Pe, 0)) 
            self.Q[-1] += self.phi_N * (-min(self.Pe, 0))
            
        else:
            # Raise an error for invalid method
            raise ValueError("Invalid method. Choose 'linear' or 'upwind'")

    def set_BC_diffusion(self):
        # Method to set boundary conditions
        
        if self.method == 'linear' or self.method == 'upwind':
            # Linear method boundary conditions for the diffiusion term 
            
            # Set coefficients for the first row in the matrix A
            self.A[0, :2] += [(4/self.Deltax),
                              (- 4/(3 * self.Deltax))]

            # Set coefficients for the last row in the matrix A
            self.A[-1, -2:] += [(- 4/(3 * self.Deltax)),
                                (4/self.Deltax)]
            
            # Set values for the first and last elements in the vector b
            self.Q[0] += self.phi_0 * (8/(3*self.Deltax)) 
            self.Q[-1] += self.phi_N * (8/(3*(self.Deltax))) 
            
        else:
            # Raise an error for invalid method
            raise ValueError("Invalid method. Choose 'linear' or 'upwind'")

    def compute_M(self):
        """
        Compute the coefficient matrix M for a finite difference scheme.

        This method constructs the coefficient matrix M used in finite difference schemes
        for solving partial differential equations. The matrix M is computed based on 
        spatial discretization and time discretization parameters.

        Returns:
        numpy.ndarray: The coefficient matrix M with shape (N, N), where N is the size 
        of the system.
        """
        
        i = np.arange(1, self.N-1)
        east_indices = i + 1
        west_indices = i - 1
        
        M = np.zeros_like(self.A)
        
        M[0, :2] = self.A[0, :2] * self.theta * self.Deltat / self.Deltax
        M[i, i] = self.A[i, i] * self.theta * self.Deltat / self.Deltax
        M[i, east_indices] = self.A[i, east_indices] * self.theta * self.Deltat / self.Deltax
        M[i, west_indices] = self.A[i, west_indices] * self.theta * self.Deltat / self.Deltax
        M[-1, 2:] = self.A[-1, 2:] * self.theta * self.Deltat / self.Deltax
        
        M = M + np.eye(self.N)
        
        return M 
        
    def compute_Nn(self):
        """
        Compute the coefficient matrix Nn for a finite difference scheme.

        This method calculates the coefficient matrix Nn used in finite difference schemes
        for solving partial differential equations. The matrix Nn is computed based on 
        spatial discretization and time discretization parameters.

        Returns:
        numpy.ndarray: The coefficient matrix Nn with shape (N, N), where N is the size 
        of the system.
        """
        
        i = np.arange(1, self.N-1)
        east_indices = i + 1 
        west_indices = i - 1 
                
        Nn = np.zeros_like(self.A)
        
        Nn[0, :2] = - self.A[0, :2] * (1- self.theta) * self.Deltat / self.Deltax
        Nn[i, i] = - self.A[i, i] * (1- self.theta) * self.Deltat / self.Deltax
        Nn[i, east_indices] = - self.A[i, east_indices] * (1- self.theta) * self.Deltat / self.Deltax
        Nn[i, west_indices] = - self.A[i, west_indices] * (1- self.theta) * self.Deltat / self.Deltax  
        Nn[-1, 2:] = - self.A[-1, 2:] * (1- self.theta) * self.Deltat / self.Deltax
        
        Nn = Nn + np.eye(self.N)
        
        return Nn  
        
    def compute_B(self):
               
        B = self.Q * self.Deltat / self.Deltax 
        
        return B        

    def solve_system(self):
        # Method to solve the system of algebraic equations [A][phi] = [b]
        # Check out np.linalg.pinv
        return np.linalg.solve(self.A, self.Q)
    
    def check_stability(self, M, Nn):
        """
        Perform eigenvalue analysis to evaluate the stability of the numerical scheme.

        Returns:
            str: A string indicating whether the scheme is stable or not.
        """
        # Compute the matrix product M^(-1) * Nn
        M_inv_Nn = np.linalg.inv(M).dot(Nn)

        # Compute eigenvalues of M^(-1) * Nn
        eigenvalues = np.linalg.eigvals(M_inv_Nn)

        # Get the absolute value of the largest eigenvalue
        max_abs_eigenvalue = np.max(np.abs(eigenvalues))

        # The scheme is stable if the absolute value of the largest eigenvalue is less than 1
        if max_abs_eigenvalue < 1:
            return True
        else:
            return False
        
    def solve_timestep(self, M, Nn, B):
        """
        Apply a function iteratively for a limited number of cycles.

        Args:
        max_cycles: Maximum number of cycles allowed.

        Returns:
        The final value after applying the function iteratively.
        """
        if self.check_stability(M, Nn):
        
            self._cycle_count = 0
            while self._cycle_count < self._max_cycles:
                try:
                    self.phi_new = np.linalg.solve(M, np.dot(Nn, self.phi_old) + B)
                except np.linalg.LinAlgError:
                    # Handle singular matrix error
                    print("Matrix M is singular. Applying alternative method...")
                    self.phi_new = np.linalg.lstsq(M, np.dot(Nn, self.phi_old) + B)[0]

                # Check for convergence
                if np.allclose(self.phi_new, self.phi_old, atol=10**(-5)):
                    print(f"Convergence achieved after {self._cycle_count} cycles.")
                    break
            
                # Store phi_old at current timestep
                self.phi_store[:, self._cycle_count] = self.phi_old  
                self.phi_old = self.phi_new
                self._cycle_count += 1
            
        # Out of the while loop we may want to add the last timestep, that is phi_new
        else:
            print(f'self.check_stablity(): {self.check_stability(M, Nn)}')
  
    def include_BC_plot(self, to_modify):   
        """
        Include boundary conditions in the solution vectors.

        This method adds boundary conditions at the start and end of the solution vectors based
        on the size of the input vector to_modify.

        Parameters:
        - to_modify (numpy.ndarray): Input vector to which boundary conditions are to be added.
          It can be of shape (N,1) or (N,max_cycles).

        Returns:
        - numpy.ndarray: Modified vector with boundary conditions added.
        
        Raises:
        - ValueError: If the shape of to_modify is not valid.
        """
        if to_modify.shape[0] != self.N:
            raise ValueError("Invalid shape for to_modify")

        if to_modify.ndim == 2:  # (N, max_cycles)
            phi_0 = np.ones((self._max_cycles)) * self.phi_0
            phi_N = np.ones((self._max_cycles)) * self.phi_N
            output = np.vstack((phi_0, to_modify, phi_N))
        elif to_modify.ndim == 1:  # (N,)
            to_modify.reshape(self.N, 1)
            phi_0 = [self.phi_0]
            phi_N = [self.phi_N]
            output = np.concatenate((phi_0, to_modify, phi_N))
        return output
        
        
    def calculate_exact_solution(self, L_values):
        # Method to calculate the exact solution
        phi = (exp(self.Pe * self.x / self.L) - 1) / (exp(self.Pe) - 1)
        phi_values = [phi.subs(self.x, val) for val in L_values]
        return [float(val.evalf()) for val in phi_values]

    def plot_solution2(self):
        """
        Plot numerical and exact solutions with legend for exact solution and a color bar indicating time step.

        Parameters:
        None

        Returns:
        None
        """
        # Number of times we want to plot
        n = 10
        # Domain size + 2 to include boundaries
        x = np.linspace(0, 1, self.N + 2)
        
        # Calculate the exact solution
        L = np.linspace(0, 1, self.N) 
        
        # Compute exact steady state solution
        phi_steady_plot = (np.array(self.calculate_exact_solution(L)))
        
        # Output of include_BC function separated into n vectors to plot
        phi_store_plot = self.include_BC_plot(self.phi_store)
        distance = self._cycle_count // n
        relevant = np.arange(0, self._cycle_count, distance)
        if len(relevant) > n:
            relevant = relevant[-n:]
        phi_store_plot = phi_store_plot[:, relevant]        

        # Define color scheme (going from lower to higher visibility)
        colors = cm.Blues(np.linspace(0.3, 0.9, phi_store_plot.shape[1]))

        # plotting
        fig, ax = plt.subplots(figsize=(5, 7))

        # Plot other solutions without legend
        for i in range(phi_store_plot.shape[1]):
            ax.plot(x.T, phi_store_plot[:, i], color=colors[i],
                    linewidth=1)

        # Plot exact steady state solution on top with legend
        ax.plot(L, phi_steady_plot, 
                label='Exact Steady State Solution', 
                color='red', 
                linestyle='-.', linewidth=3)

        # Add legend for exact solution
        ax.legend()

        # Add color bar indicating time step
        norm = mcolors.Normalize(vmin=0, vmax=phi_store_plot.shape[1])
        sm = cm.ScalarMappable(cmap='Blues', norm=norm)
        sm.set_array([])
        cbar = fig.colorbar(sm, ax=ax, label='Grade of iteration progress')
        
        names = {'0': 'Forward Euler', '1': 'Backward Euler', '0.5': 'Crank Nicolson'}
        
        text = f'{names[str(self.theta)]} - N:{self.N} - Pe:{self.Pe} - method:{self.method} - C:{self.C} - dt:{self.Deltat}'
        ax.text(0.5, -0.1, text, ha='center', va='center', 
                transform=ax.transAxes, bbox=dict(facecolor='white', alpha=0.5))
        
        # Create directory to save plot
        os.makedirs(self.save_folder, exist_ok=True)
        # Generate unique file name
        figure_name = f'{self.method}_method_Pe_{self.Pe}_N{self.N}_theta{self.theta}.png'
        # Define save path
        save_path = os.path.join(self.save_folder, figure_name)
        # Save plot with legend and color bar included
        plt.savefig(save_path, bbox_inches='tight')


### The following only needed for Assignment01  
    def plot_solution(self, numerical_solution, exact_solution):
        # Method to plot the numerical and exact solutions along with the exact solution plot
        y = np.linspace(0, self.L, self.N)

        plt.figure()
        plt.plot(y, numerical_solution, label=f'Numerical Solution ({self.method} method) for Pe {self.Pe}')
        plt.plot(y, exact_solution, label='Analitical Solution', linestyle='--')
        plt.xlabel('x')
        plt.ylabel('phi(x)')
        plt.legend()

        # Ensure the save folder exists
        os.makedirs(self.save_folder, exist_ok=True)

        # Generate the figure name based on the label of the numerical solution so that every figure_name is unique
        figure_name = f'{self.method}_method_Pe_{self.Pe}_N{self.N}_{numerical_solution[0]:.2f}_{numerical_solution[-1]:.2f}.png'
        save_path = os.path.join(self.save_folder, figure_name)

        plt.savefig(save_path)
        plt.close()

    def calculate_error(self, method, max_N=1000):
        # Method to calculate global error for both linear and upwind solutions
        errors = []
        N_values = np.arange(10, max_N + 1, 100)
        self.method = method

        for N in N_values:
            self.N = N
            self.Deltax = self.L/self.N
            self.A = np.zeros((N, N))
            self.Q = np.zeros(N)

            self.construct_diffusive_matrix()
            self.set_boundary_conditions()

            # Solve the system
            numerical_solution = self.solve_system()

            # Discretized length values
            L_values = np.linspace(0, self.L, N)

            # Calculate the exact solution
            exact_solution = self.calculate_exact_solution(L_values)

            # Calculate mean absolute error
            error = np.mean(np.abs(exact_solution - numerical_solution))
            errors.append(error)

        return N_values, errors

    def plot_error(self, linear_errors, upwind_errors, order_linear, order_upwind):
        # Method to plot the double-logarithmic scale of global error
        fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 10))

        # Calculate grid spacing (self.Deltax) for each N
        deltax_values_linear = 1 / (linear_errors[0] - 1)
        deltax_values_upwind = 1 / (upwind_errors[0] - 1)

        # Plot using grid spacing on the x-axis
        axes[0].loglog(deltax_values_linear, linear_errors[1], label='Linear Method')
        axes[0].loglog(deltax_values_upwind, upwind_errors[1], label='Upwind Method')

        axes[0].set_xlabel('Grid Spacing (Deltax)')
        axes[0].set_ylabel('Mean Absolute Error')
        axes[0].legend()
        axes[0].set_title('Global Error vs. Grid Spacing')

        # Plot the order of error in log scale
        axes[1].loglog(deltax_values_linear, order_linear, label='Linear Method')
        axes[1].loglog(deltax_values_upwind, order_upwind, label='Upwind Method')

        axes[1].set_xlabel('Grid Spacing (Deltax)')
        axes[1].set_ylabel('Order of discretization')
        axes[1].legend()
        axes[1].set_title('Estimate order of discretization vs. Grid Spacing')

        # Ensure the save folder exists
        os.makedirs(self.save_folder, exist_ok=True)

        # Save the figure
        figure_name = f'error_plot_Pe_{self.Pe}_Deltax.png'
        save_path = os.path.join(self.save_folder, figure_name)

        plt.savefig(save_path)
        plt.close()

    def estimate_order_of_error(self, errors):
        # Method to estimate the overall order of discretization error

        # Extract grid points (log_N) and corresponding errors (log_error)
        log_N = np.log(errors[0])
        log_Deltax = np.log(1 / (errors[0] - 1))
        log_error = np.log(errors[1])

        return log_error/log_Deltax

    def solve_and_plot_with_error(self, max_N=1000):
        # Method to perform computations, display the plot, and estimate the order of error
        linear_errors = self.calculate_error(method='linear', max_N=max_N)
        upwind_errors = self.calculate_error(method='upwind', max_N=max_N)

        # Estimate the order of error
        order_linear = self.estimate_order_of_error(linear_errors)
        order_upwind = self.estimate_order_of_error(upwind_errors)


        # Plot the errors and slopes
        self.plot_error(linear_errors, upwind_errors, order_linear, order_upwind)
        